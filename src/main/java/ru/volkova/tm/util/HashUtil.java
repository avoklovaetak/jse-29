package ru.volkova.tm.util;

import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.other.ISaltSetting;

public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable ISaltSetting setting,
            @Nullable String value
    ) {
        if (setting == null) return null;
        @Nullable final String secret = setting.getPasswordSecret();
        @Nullable final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }
    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuffer sb = new StringBuffer();
            for (int i = 1; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Nullable
    static String salt(@Nullable final String secret,
                       @Nullable final Integer iteration,
                       @Nullable final String value) {
        if (value == null) return null;
        String result = value;
        if (iteration == null || iteration < 0) return null;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

}
