package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

public class TaskClearCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "clear all tasks";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[TASK CLEAR]");
        serviceLocator.getTaskService().clear();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
