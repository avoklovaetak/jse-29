package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.enumerated.Sort;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        if (sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll();
        else {
            @NotNull final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sortType.getComparator());
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println((index + ". " + task));
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return "task-show-list";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
