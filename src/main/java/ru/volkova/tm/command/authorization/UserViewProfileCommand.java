package ru.volkova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.UserNotFoundException;

import java.util.Optional;

public class UserViewProfileCommand extends AbstractAuthCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "view user profile";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[VIEW PROFILE]");
        @NotNull final Optional<User> user = serviceLocator.getAuthService().getUser();
        if (!user.isPresent()) throw new UserNotFoundException();
        System.out.println("LOGIN: " + user.get().getLogin());
        System.out.println("E-MAIL: " + user.get().getEmail());
        System.out.println("FIRST NAME: " + user.get().getFirstName());
        System.out.println("LAST NAME: " + user.get().getSecondName());
        System.out.println("MIDDLE NAME" + user.get().getMiddleName());
    }

    @Override
    public String name() {
        return "user-view-profile";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
