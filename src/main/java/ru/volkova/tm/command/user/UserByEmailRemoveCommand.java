package ru.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class UserByEmailRemoveCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "remove user by email";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByEmail(email);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-email";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
