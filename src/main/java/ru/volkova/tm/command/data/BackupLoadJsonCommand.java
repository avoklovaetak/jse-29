package ru.volkova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Domain;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadJsonCommand extends AbstractDataCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String description() {
        return "Json backup data load";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final File file = new File(FILE_BACKUP);
        if (!file.exists()) return;
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_BACKUP)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    public @Nullable String name() {
        return "backup-load";
    }

}
