package ru.volkova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.dto.Domain;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

public abstract class AbstractDataCommand extends AbstractCommand {

    protected static final String FILE_BACKUP = "./backup.json";

    protected static final String FILE_BINARY = "./data.bin";

    protected static final String FILE_BASE64 = "./data.base64";

    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";

    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";

    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    protected static final String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";

    protected static final String JAXB_JSON_PROPERTY_VALUE = "application/json";

    protected static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";

    protected static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public Domain getDomain() {
        final Domain domain = new Domain();
        if (serviceLocator == null) throw new ObjectNotFoundException();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) return;
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
