package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void clear() {
        entities.clear();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entities;
    }

    @NotNull
    @Override
    public Optional<User> findByEmail(@NotNull final String email) {
        return entities.stream()
                .filter(predicateByEmail(email))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<User> findById(@NotNull final String id) {
        return entities.stream()
                .filter(predicateById(id))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<User> findByLogin(@NotNull final String login) {
        return entities.stream()
                .filter(predicateByLogin(login))
                .findFirst();
    }

    @Override
    public void lockByEmail(@NotNull String email) {
        findByEmail(email).ifPresent(user -> user.setLocked(false));
    }

    @Override
    public void lockById(@NotNull String id) {
        findById(id).ifPresent(user -> user.setLocked(false));
    }

    @Override
    public void lockByLogin(@NotNull String login) {
        findByLogin(login).ifPresent(user -> user.setLocked(false));
    }

    @NotNull
    protected final Predicate<User> predicateByEmail(@NotNull String email) {
        return user -> email.equals(user.getEmail());
    }

    @NotNull
    protected final Predicate<User> predicateByLogin(@NotNull String login) {
        return user -> login.equals(user.getLogin());
    }

    @Override
    public void remove(@NotNull final User user) {
        entities.remove(user);
    }

    @Override
    public void removeByEmail(@NotNull String email) {
        findByEmail(email).ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull final String id) {
        findById(id).ifPresent(this::remove);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        findByLogin(login).ifPresent(this::remove);
    }

    @Override
    public void unlockByEmail(@NotNull String email) {
        findByEmail(email).ifPresent(user -> user.setLocked(true));
    }

    @Override
    public void unlockById(@NotNull String id) {
        findById(id).ifPresent(user -> user.setLocked(true));
    }

    @Override
    public void unlockByLogin(@NotNull String login) {
        findByLogin(login).ifPresent(user -> user.setLocked(true));
    }

}
