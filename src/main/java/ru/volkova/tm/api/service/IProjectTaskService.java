package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Task;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskByProjectId(
            @NotNull String userId,
            @Nullable String projectId,
            @NotNull String taskId
    );

    @NotNull
    List<Task> findAllTasksByProjectId(@NotNull String userId, @Nullable String projectId);

    void removeProjectById(@NotNull String userId, @Nullable String id);

    void unbindTaskByProjectId(
            @NotNull String userId,
            @Nullable String projectId,
            @NotNull String taskId
    );

}
