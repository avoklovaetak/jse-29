package ru.volkova.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.entity.IHasDateStart;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByDateStart implements Comparator<IHasDateStart> {

    @NotNull
    private final static ComparatorByDateStart INSTANCE = new ComparatorByDateStart();

    @NotNull
    public static ComparatorByDateStart getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasDateStart o1, final IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
