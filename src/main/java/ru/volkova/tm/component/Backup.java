package ru.volkova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.command.data.BackupLoadJsonCommand;
import ru.volkova.tm.command.data.BackupSaveJsonCommand;

public class Backup extends Thread {

    @NotNull
    final Bootstrap bootstrap;

    private static final int INTERVAL = 30000;

    @NotNull
    private static final String BACKUP_SAVE = "backup-save";

    @NotNull
    private static final String BACKUP_LOAD = "backup-load";

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommand(new BackupLoadJsonCommand().name());
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void save(){
        bootstrap.parseCommand(new BackupSaveJsonCommand().name());
    }

}
